# README #
angularjs1.5에서 karma+jasmine 단위 테스트 해 봅니다.

## angularjs1.5( 테스트 할 것 들 ) ##
* controller 
* $http 
* directive
* filter
* service
* factory

## karma ###
* 기본 http-server 이용.
* jasmine을 이용한 테스트.
* karma-spec-reporter 로 개별 테스트
* karma-coverage 로 coverage 별도 생성
* PhantomJS로 헤드리스 환경으로 테스트


### jasmine ###
* 단위테스트를 작성할 때 준비/행동/확인 패턴
* 준비 - 테스트에 필요한 시나리오를 설정하는 과정
* 행동 - 실제 테스트를 수행하는 과정
* 확인은 올바른 결과가 나왔는지 검사하는 과정

### jasmine 함수 ###
* describe - 관련 테스트 여러 개를 그룹으로 지정.테스트 코드를 조직화하는 데 도움.
* beforeEach - 각 테스트 전에 함수를 실행(준비과정)
* it - 테스트를 수행하기 위한 함수를 실행(행동단계)
* expect - 테스트 결과를 확인 (확인단계)
* toEqual - 테스트 결과를 예상 결과와 비교한다.( 테스트 확인의 또 다른 단계 )

### 결과 확인을 위한 jasmine 함수 ###
* expect(x).toBeFalsy() - x가 false이거나 false로 평가되는지 확인.
* expect(x).toEqual(val) - x가 val과 같은 값을 갖는지 확인한다.( 같은 객체일 필요는 없다 )
* expect(x).toBe(obj) - x 및 obj가 같은 객체인지 확인.
* expect(x).toMatch( regexp ) - x가 지정한 정규식과 일치하는지 확인.
* expect(x).toBeDefined() - x가 정의돼 있는지 확인.
* expect(x).toBeUndefined() - x가 정의돼 있지 않은지 확인.
* expect(x).toBeTruthy() - x가 true이거나 true 로 평가되는지 확인.
* expect(x).toBeFalsy() - x가 false이거나 false로 평가되는지 확인.
* expect(x).toContain(y) - x가 y를 포함하는 문자열인지 화인한다.
* expect(x).toBeGreaterThan(y) - x가 y보다 큰지 확인. 

### tip ###
$httpBackend 호출시 함수에 $injector 를 매개변수로 가져와 $injector.get('$httpBackend'); 이런식으로 호출. 변수에 대입시키면 된다.
혹은 _$httpBackend_라고 매개변수를 선언하여도 된다.
