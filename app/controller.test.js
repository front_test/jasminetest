describe('Controller Test', function() {
    //준비
    var mockScope = {};
    var controller;
    var httpBackend;

    beforeEach(angular.mock.module('jasmineTestApp'));
    beforeEach(inject(function($injector) {
        httpBackend= $injector.get('$httpBackend');
        //기대한 요청에 대한 응답을 정의한다.
        httpBackend.expect('GET', 'productData.json').respond(
            [
                {'name':'Apples', 'category':'Fruit', 'price':1.20 },
                {'name':'Bannas', 'category':'Fruit', 'price':2.42 },
                {'name':'Pears', 'category':'Fruit', 'price':2.02 },
            ]
        )
    }));
    beforeEach(inject(function($controller, $rootScope, $http) {
        mockScope=$rootScope.$new()
        controller=$controller('defaultCtrl', {
            $scope:mockScope,
            $http:$http
        });
        httpBackend.flush(); //지정한 컨트롤러의 인스턴스를 생성한다.
    }));

    it('should controller', function() {
        expect(controller).toBeDefined();
    });

    it('create counter', function() {
        mockScope.incrementCounter();
        expect(mockScope.counter).toEqual(1);
    });

    it('Make an Ajax request', function() {
        httpBackend.verifyNoOutstandingExpectation();//기대한 요청이 모두 수신됐는지 검사한다
    });
    it('Progresses the data', function() {
        expect(mockScope.products).toBeDefined();
        expect(mockScope.products.length).toEqual(3);
    });
    it('Preserves the data order', function() {
        expect(mockScope.products[0].name).toEqual('Apples');
    })

})
