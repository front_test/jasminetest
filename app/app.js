angular.module('jasmineTestApp', [])
    .controller('defaultCtrl', function($scope, $http) {
        $scope.counter=0;
        //$scope.products
        $http.get('productData.json').then(function(res) {
            $scope.products=res.data;
        }).catch(function(error) {
            console.log(error);
        });
        $scope.incrementCounter=function() {
            $scope.counter++;
        };
    })
/*var mockScope = {};
var controller;
beforeEach(angular.mock.module('jasmineTestApp'));
beforeEach(angular.mock.inject(function($controller, $rootScope) {
    mockScope=$rootScope.$new();
    controller=$controller('defaultCtrl', {
        $scope:mockScope
    });
}));

it('should.....', function() {
    expect( controller ).toBeDefined();
});
//행동 및 확인
it('Create variable', function() {
    expect( mockScope.counter ).toEqual(0);
});
it('Increments counter', function() {
    mockScope.incrementCounter();
    expect( mockScope.counter ).toEqual(1);
});*/
